Part A

Generate Private key RSA 2048

openssl genrsa -aes256 -out private-key.pem 2048

Give a passphrase : Freedom


View Private key parameters

openssl rsa -text -in private-key.pem


Create corresponding Public Key

openssl rsa -in private-key.pem -pubout -out public-key.pem


Encrypt a file using Public key

openssl rsautl -encrypt -pubin -inkey public-key.pem  -in plain.txt -out encr.txt

Decrypt using Private key
openssl rsautl -decrypt -inkey private-key.pem -in encr.txt -out decr.txt


Signing using Private key
openssl dgst -sha512 -sign private-key.pem -out digest.sha512 plain.txt

Verify using Public key
openssl dgst -sha512 -verify public-key.pem -signature digest.sha512 plain.txt


Creating SB.key as Bob
putta:openssl$ cat > SB.key
aes256
Rebel
2048


Encrypt using AES 256 CBC iter 2048
openssl enc -aes-256-cbc -pbkdf2 -a  -iter 2048 -in inputfile -out outputfile

Decrypt using AES 256 CBC iter 2048

openssl enc -aes-256-cbc -pbkdf2 -a -d -iter 2048 -in inputfile -out outputfile

Part B



Generate private and public key pair

openssl genrsa -aes128 -out fd.key 2048

openssl rsa -in fd.key -pubout -out fd-public.key

Generate CA certificate
openssl req -newkey rsa:2048 -nodes -keyout fd.key -x509 -days 365 -out charlie-ca.crt


Bob generates CSR
openssl req -new -key fd.key -out bob-browser.csr

Charlie provides bob-browser.crt
openssl x509 -req -CA charlie-ca.crt -days 365 -in bob-browser.csr -CAkey fd.key -CAcreateserial -out bob-browser.crt

Verifying the bob-browser.crt
openssl verify -verbose -CAfile charlie-ca.crt bob-browser.crt
